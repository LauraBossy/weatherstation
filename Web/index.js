function currentWeather(humiditydata, temperaturedata) {
    let humidity = humiditydata;
    let temp = temperaturedata;
    // rain
    if (humidity > 70 && temp >= 0) {
        document.getElementById('rain').style.display = 'flex';
        document.getElementById('sun').style.display = 'none';
        document.getElementById('snow').style.display = 'none';
    }
    // snow
    else if (humidity > 70 && temp < -1) {
        document.getElementById('rain').style.display = 'none';
        document.getElementById('sun').style.display = 'none';
        document.getElementById('snow').style.display = 'flex';
    }
    // sun
    else if ((humidity < 69 && temp >= 0) || (humidity < 69 && temp < 1)) {
        document.getElementById('rain').style.display = 'none';
        document.getElementById('sun').style.display = 'flex';
        document.getElementById('snow').style.display = 'none';
    }
}

function UpdateChartAsync() {
    var urlToCall = location.protocol.concat("//").concat(window.location.host).concat("/cubes2/weatherstation-api/dailyreports");
    fetch(urlToCall).then((resp) => resp.text()).then(
        function (Probes) {
            UpdateChartDisplay(Probes);
            setTimeout(function () { UpdateChartAsync(); }, 1000);
        }
    )
}

// Display values in the current chart
function UpdateChartDisplay(ProbesJson) {
    var Probes = JSON.parse(ProbesJson);
    var tempArray = [];
    var humArray = [];
    var TempTotal = 0;
    var HumTotal = 0;
    for (let i = 0; i < Probes.length; i++) {
        tempArray.push(Probes[i].Temperature.toFixed(2))
        humArray.push(Probes[i].Humidity.toFixed(2))
        TempTotal += Probes[i].Temperature;
        HumTotal += Probes[i].Humidity;
    }
    var TempAvg = (TempTotal / Probes.length).toFixed(2);
    var HumAvg = (HumTotal / Probes.length).toFixed(2);
    document.getElementById('temperature').innerHTML = TempAvg + " °C ";
    document.getElementById('humidity').innerHTML = HumAvg + " % ";

    // display the picto depend on average weather
    currentWeather(HumAvg, TempAvg);

    let currentData = {
        labels: [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
        datasets: [{
            label: 'Temperature',
            backgroundColor: 'red',
            borderColor: 'red',
            data: tempArray.reverse(),
        }, {
            label: 'Humidity',
            backgroundColor: 'blue',
            borderColor: 'blue',
            data: humArray.reverse()
        }]
    };

    let currentChart = Chart.getChart("currentChart");
    if (currentChart != undefined) {
        currentChart.data = currentData;
        currentChart.update();
    } else {
        let currentConfig = {
            type: 'line',
            data: currentData,
            options: {
                animations: false
            }
        };
        let ctx = document.getElementById('currentChart').getContext('2d');
        currentChart = new Chart(ctx, currentConfig);
    }
}

// Display Date
function DisplayDate() {
    var today = new Date();
    var day = today.getDay();
    var daylist = ["Sunday", "Monday", "Tuesday", "Wednesday ", "Thursday", "Friday", "Saturday"];
    //  var date = '0' + today.getDate() + '/0' + (today.getMonth()+1) + '/' + today.getFullYear(); // => '0' before the tenth of the month
    var date = today.getDate() + '/0' + (today.getMonth() + 1) + '/' + today.getFullYear();
    document.getElementById("displayDateTime").innerHTML = 'Weather for ' + daylist[day] + ' ' + date;
}