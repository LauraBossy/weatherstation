<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title> Weather of the week </title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="index.css">
    </head>
    <body onload="ValuesToWeekChart();">

        <section class="vh-100" style="background-color: #cdc4f9;"> 
            <ul class="background">
                <li> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="white" class="bi bi-cloudy-fill" viewBox="0 0 16 16"><path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/></svg> 
                </li>
                <li> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="#D3D3D3" class="bi bi-cloudy-fill" viewBox="0 0 16 16"><path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/></svg> 
                </li>
                <li> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="#D3D3D3" class="bi bi-cloudy-fill" viewBox="0 0 16 16"><path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/></svg> 
                </li>
                <li> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="#F0F8FF" class="bi bi-cloudy-fill" viewBox="0 0 16 16"><path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/></svg> 
                </li>
                <li> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="#F0FFFF" class="bi bi-cloudy-fill" viewBox="0 0 16 16"><path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/></svg> 
                </li>
                <li> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="#F0F8FF" class="bi bi-cloudy-fill" viewBox="0 0 16 16"><path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/></svg> 
                </li>
                <li> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="#FFFAF0" class="bi bi-cloudy-fill" viewBox="0 0 16 16"><path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/></svg> 
                </li>
                <li> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="#F8F8FF" class="bi bi-cloudy-fill" viewBox="0 0 16 16"><path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/></svg> 
                </li>
                <li> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="#F5FFFA" class="bi bi-cloudy-fill" viewBox="0 0 16 16"><path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/></svg> 
                </li>
            </ul>
            <div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-md-12 col-xl-12">
                        <div class="card shadow-0 border border-dark border-5 text-dark" style="border-radius: 10px;">
                            <div class="card-body p-4">
                                <div class="row text-center">
                                    <div class="col-md-12 text-center border-end border-5 border-black py-4" style="margin-top: -1.5rem; margin-bottom: -1.5rem;">
                                        <div class="d-flex justify-content-around mt-3">
                                            <h1> Weekly weather forecast </h1>
                                        </div>
                                        <div>
                                            <p id="data"></p>
                                        </div>
                                        <div class="d-flex justify-content-around align-items-center py-5 my-4" style="height: 100%; display:flex; flex-direction: column; justify-content: space-between;">
                                            <div style="display:flex; flex-direction: row; width: 100%; justify-content: center;">
                                                <div style="width: 70%;">
                                                    <canvas id="myweekChart"></canvas>
                                                </div> 
                                            </div>
                                            <div style="align-self: flex-end; width: 20%; display: flex; flex-direction: row;">
                                                <div>                                            
                                                    <a id="whatsapp" href="whatsapp://send?text= Let's check the weather ! http://192.168.43.176/IHM/weekChart.php" data-action="share/whatsapp/share" target="_blank">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="30%" fill="#ee7168" class="bi bi-share-fill" viewBox="0 0 16 16">
                                                        <path d="M11 2.5a2.5 2.5 0 1 1 .603 1.628l-6.718 3.12a2.499 2.499 0 0 1 0 1.504l6.718 3.12a2.5 2.5 0 1 1-.488.876l-6.718-3.12a2.5 2.5 0 1 1 0-3.256l6.718-3.12A2.5 2.5 0 0 1 11 2.5z"/>
                                                        </svg>                                            
                                                    </a>
                                                    <p style="font-size: 10px; margin-top: 10px; color: #ee7168;"> Share with WhatsApp </p>
                                                </div> 
                                                <a href="index.php">
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 45.58 45.58" style="enable-background:new 0 0 45.58 45.58; width: 30%;" xml:space="preserve">
                                                        <g>
                                                            <path d="M45.506,33.532c-1.741-7.42-7.161-17.758-23.554-19.942V7.047c0-1.364-0.826-2.593-2.087-3.113
                                                                c-1.261-0.521-2.712-0.229-3.675,0.737L1.305,19.63c-1.739,1.748-1.74,4.572-0.001,6.32L16.19,40.909
                                                                c0.961,0.966,2.415,1.258,3.676,0.737c1.261-0.521,2.087-1.75,2.087-3.113v-6.331c5.593,0.007,13.656,0.743,19.392,4.313
                                                                c0.953,0.594,2.168,0.555,3.08-0.101C45.335,35.762,45.763,34.624,45.506,33.532z"/>
                                                        </g>                                                
                                                    </svg>                                                    
                                                </a>
                                            </div>                                                                                                                                                                     
                                        </div>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body> 

<script src="weekChart.js"></script> 

</html>