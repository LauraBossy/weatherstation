// Display values in the week chart
function ValuesToWeekChart() {
    let weekChart = document.getElementById('myweekChart').getContext('2d');

    // Chart where arrays are displayed
    let theWeekChart = new Chart(weekChart, {
        type: 'line',
        data: {
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            datasets: [{
                label: 'Temperature',
                backgroundColor: 'red',
                borderColor: 'red',
                data: [12, 10, 16, 18, 16, 18, 14],
            }, {
                label: 'Humidity',
                backgroundColor: 'blue',
                borderColor: 'blue',
                data: [93, 91, 81, 77, 60, 52, 36],
            }],
        }
    });
}

function Capture() {
    html2canvas(document.body).then((canvas) => {
        let a = document.createElement("a");
        a.download = "ss.png";
        a.href = canvas.toDataURL("image/png");
        a.click();
    });
}