<?php

require_once("./api.php");

try {

    if (!empty($_GET['servicename'])) {

        $url = explode("/", filter_var($_GET['servicename'], FILTER_SANITIZE_URL)); 
        //var_dump($url[1]);
        //die();

        switch ($url[0]) {

            case "newreport":
                if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
                    throw new Exception("The request method is not valid, please use POST");
                }
                postReport();
                break;

            case "reports":
                if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
                    throw new Exception("The request method is not valid, please use GET");
                }
                if (empty($url[1])) {
                    getreports();
                } else {
                    
                    getReportsByProbe($url[1]);
                }
                break;

            case "dailyreports":
                if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
                    throw new Exception("The request method is not valid, please use GET");
                }
                getDailyReports();
                break;

            case "probes":
                if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
                    throw new Exception("The request method is not valid, please use GET");
                }                
                getProbes();
                break;

            default:
                throw new Exception("The request is not valid, check url");
        }
    } else {
        throw new Exception('Data recovery problem');
    }
} catch (Exception $e) {
    $erreur = [
        "message" => $e->getMessage(),
        "code" => $e->getCode()
    ];
    sendJson($erreur);
}
